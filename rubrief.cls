% RUBRIEF.CLS August 2004 Author: Ben Polman
\NeedsTeXFormat{LaTeX2e} \ProvidesClass{rubrief} \RequirePackage{ifthen}
\newboolean{@color} \setboolean{@color}{false}
\newboolean{@logo} \setboolean{@logo}{false}
\newboolean{@dutch} \setboolean{@dutch}{false}
\DeclareOption{dutch}{\setboolean{@dutch}{true}}
\DeclareOption{logo}{\setboolean{@logo}{true}}
\DeclareOption{color}{\setboolean{@color}{true}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{letter}} \ProcessOptions
\LoadClass[a4paper]{letter}
%\RequirePackage{helvet}
\ifcase\pdfoutput \RequirePackage[dvips]{graphicx} \else
\RequirePackage[pdftex]{graphicx}
\DeclareGraphicsExtensions{.jpg,.pdf,.mps,.png} \fi
\newcommand{\englishnames}{%
 \newcommand{\yrefname}{Your Ref.}
 \newcommand{\myrefname}{Our Ref.}
 \newcommand{\subjectname}{Subject}
 \newcommand{\datename}{Date}
 \newcommand{\telephonename}{Telephone}
 \newcommand{\doorkies}{Telephone}
 \newcommand{\dutchname}{The Netherlands}
}
\newcommand{\dutchnames}{%
%. . . . . . . . . . . . . . . . . . . . .
 \newcommand{\yrefname}{Uw kenmerk}
 \newcommand{\myrefname}{Ons kenmerk}
 \newcommand{\subjectname}{Betreft}
 \newcommand{\telephonename}{Telefoon}
 \newcommand{\doorkies}{Doorkiesnummer}
 \newcommand{\datename}{Datum}
 \newcommand{\dutchname}{}
}
\ifthenelse{\boolean{@dutch}}
  {\RequirePackage[dutch]{babel}\dutchnames}{\englishnames}
\newcommand{\@mainhead}{}
\newcommand{\@subhead}{}
\newcommand{\@myaddress}{}
\newcommand{\@projectcode}{}
\newcommand{\@yref}{}
\newcommand{\@myref}{}
\newcommand{\@subject}{}
\newcommand{\@web}{}
\newcommand{\@btw}{}
\newcommand{\@kvk}{}
\newcommand{\@email}{}
\newcommand{\@extension}{}
\newcommand{\@telephone}{}
\newcommand{\@fax}{}
\newcommand{\mainhead}[1]{\renewcommand{\@mainhead}{#1}}
\newcommand{\subhead}[1]{\renewcommand{\@subhead}{#1}}
\newcommand{\myaddress}[1]{\renewcommand{\@myaddress}{#1}}
\newcommand{\projectcode}[1]{\renewcommand{\@projectcode}{#1}}
\newcommand{\yref}[1]{\renewcommand{\@yref}{#1}}
\newcommand{\myref}[1]{\renewcommand{\@myref}{#1}}
\newcommand{\subject}[1]{\renewcommand{\@subject}{#1}}
\renewcommand{\telephone}[1]{\renewcommand{\@telephone}{#1}}
\newcommand{\extension}[1]{\renewcommand{\@extension}{#1}}
\newcommand{\fax}[1]{\renewcommand{\@fax}{#1}}
\newcommand{\web}[1]{\renewcommand{\@web}{#1}}
\newcommand{\btw}[1]{\renewcommand{\@btw}{#1}}
\newcommand{\kvk}[1]{\renewcommand{\@kvk}{#1}}
\newcommand{\email}[1]{\renewcommand{\@email}{#1}}
\def\@texttop{\relax}
\setlength{\textheight}{225mm}\setlength{\textwidth}{160mm}
\setlength{\hoffset}{-1in} \setlength{\voffset}{-1in} 
\setlength{\oddsidemargin}{25mm}\setlength{\evensidemargin}{25mm}
\setlength{\topmargin}{21mm}\addtolength{\topmargin}{-16pt}
\setlength{\footskip}{20mm}
\setlength{\parindent}{0pt}
\setlength{\fboxsep}{0pt}
\DeclareFixedFont{\viisf}{OT1}{phv}{m}{n}{7}
\DeclareFixedFont{\viiisf}{OT1}{phv}{m}{n}{8}
\DeclareFixedFont{\ixsfb}{OT1}{phv}{b}{n}{9}
\DeclareFixedFont{\ixsf}{OT1}{phv}{m}{n}{9}
\DeclareFixedFont{\xsf}{OT1}{phv}{m}{n}{10}
\DeclareFixedFont{\xiiisfb}{OT1}{phv}{b}{n}{13}
\DeclareFixedFont{\ixrm}{OT1}{ptm}{m}{n}{9}
\DeclareFixedFont{\xrm}{OT1}{ptm}{m}{n}{10}
\DeclareFixedFont{\xirm}{OT1}{ptm}{m}{n}{11}
\DeclareFixedFont{\xiiirm}{OT1}{ptm}{m}{n}{13}
%%% ugly hack to provide euro symbol in rubrief class
%%% without the need to include marvosym package to avoid
%%% name clash with fax command
\newcommand{\mvshack}{\fontfamily{mvs}\fontencoding{U}%
\fontseries{m}\fontshape{n}\selectfont}
\newcommand{\mvhackchr}[1]{{\mvshack\char#1}}
\newcommand\euro{\mvhackchr{101}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newlength{\leftfield}    \setlength{\leftfield}{120mm}
\newlength{\rightfield}   \setlength{\rightfield}{40mm}
\newsavebox{\HEAD}       \newsavebox{\firmaddress}
\newsavebox{\firmhead}   \newsavebox{\firmfoot}
\newcommand{\@createheader}{%
\sbox{\HEAD}{\smash{\parbox[t]{\leftfield}
       {\xiiisfb \@mainhead}}}
\sbox{\firmaddress}{\smash{\parbox[t]{\rightfield}{{\ixsfb\setlength{\baselineskip}{11pt}
    \@subhead}\\[\baselineskip]%
    \viiisf\setlength{\baselineskip}{11pt}\@myaddress
    \ifthenelse{\boolean{@dutch}}
    {\relax}{\\ \dutchname}\\[\baselineskip]
    \ifthenelse{\equal{\@telephone}{}}
         {}{{\telephonename{} \@telephone}}
    \ifthenelse{\equal{\@fax}{}}
         {}{\\{Fax \@fax}}
    \ifthenelse{\equal{\@web}{}}
         {}{\\[\baselineskip] {\@web}}
    \ifthenelse{\equal{\@btw}{}}
         {}{\\[\baselineskip] {\@btw}}
    \ifthenelse{\equal{\@kvk}{}}
         {}{\\ {\@kvk}}
    }}}%
\sbox{\firmhead}{\parbox{\textwidth}{\usebox{\HEAD}%
     {\usebox{\firmaddress}}}}}
\sbox{\firmfoot}
{\ifthenelse{\boolean{@logo}}
    {\ifthenelse{\boolean{@color}}
       {\ifthenelse{\boolean{@dutch}}
          {\parbox{\textwidth}{\includegraphics[width=9.9cm]{ru-nl-cmyk-a4}}}
          {\parbox{\textwidth}{\includegraphics[width=9.9cm]{ru-eng-cmyk-a4}}}}
       {\ifthenelse{\boolean{@dutch}}
          {\parbox{\textwidth}{\includegraphics[width=9.9cm]{ru-nl-zwart-a4}}}
          {\parbox{\textwidth}{\includegraphics[width=9.9cm]{ru-eng-zwart-a4}}}}}
  {\relax}}
\renewcommand{\ps@firstpage}{\setlength{\headheight}{16pt}
\setlength{\headsep}{12mm}%
\renewcommand{\@oddhead}{\usebox{\firmhead}}%
\renewcommand{\@oddfoot}{\usebox{\firmfoot}}%
\renewcommand{\@evenhead}{}\renewcommand{\@evenfoot}{}}
\renewcommand{\ps@headings}{\setlength{\headheight}{5mm}%
\setlength{\topskip}{2\baselineskip}%
\setlength{\headsep}{14.1mm}%
\renewcommand{\@oddhead}
{\smash{\parbox{\textwidth}{\setlength{\tabcolsep}{0mm}%
\begin{tabular}{p{40mm}p{40mm}p{80mm}}
          \viisf\myrefname&\viisf\pagename&\viisf\datename\\
	   \xirm\@myref&\xirm\thepage&\xirm\@date
\end{tabular}}}%
}%
\renewcommand{\@oddfoot}{\usebox{\firmfoot}}
\renewcommand{\@evenhead}{\@oddhead}
\renewcommand{\@evenfoot}{\@oddfoot}}
\providecommand{\@evenhead}{}\providecommand{\@oddhead}{}
\providecommand{\@evenfoot}{}\providecommand{\@oddfoot}{}
\pagestyle{headings}
\renewenvironment{letter}[2][empty]
  {\newpage
   \ifthenelse{\equal{#1}{empty}}
    {\relax}{\input #1.tex}
    \@createheader
    \if@twoside \ifodd\c@page
                \else\thispagestyle{empty}\null\newpage\fi
    \fi
    \c@page \@ne
    \c@footnote \z@
    \interlinepenalty=200 % smaller than the TeXbook value
    \@processto{\leavevmode\ignorespaces #2}}
  {\stopletter\@@par\pagebreak\@@par
    \if@filesw
      \begingroup
        \let\\=\relax
        \let\protect\@unexpandable@protect
        \immediate\write\@auxout{%
          \string\@mlabel{\returnaddress}{\toname\\\toaddress}}%
      \endgroup
    \fi}
\renewcommand{\opening}[1]{\thispagestyle{firstpage}%
\parbox[t]{\leftfield}{
\parbox[b][4.2cm][c]{\leftfield}{\xirm
\rlap{\raisebox{-2mm}\mbox{\ixrm\@projectcode}}
\setlength{\baselineskip}{13pt}
\toname\\\toaddress}}%
\vspace*{14mm}
\parbox[t]{\rightfield}{\ }
\parbox{\textwidth}{\setlength{\tabcolsep}{0mm}%
\begin{tabular}{p{40mm}p{40mm}p{40mm}p{39mm}}
           \viisf\myrefname&\viisf\yrefname&\viisf\doorkies&\viisf\datename\\
	   \xirm\@myref&\xirm\@yref&\xirm\@extension&\xirm\@date\\
	   \viisf\subjectname&&\viisf E-mail\\
	   \multicolumn{2}{l}{\xirm\@subject}&\xirm\@email
\end{tabular}
}%
\vspace{3\parskip} \xirm #1 \\[0mm]
\ifthenelse{\boolean{@dutch}}{\selectlanguage{dutch}}{\relax}
\xirm\setlength{\baselineskip}{12pt}
\raggedright
\nobreak}
\renewcommand{\closing}[1]{\par\nobreak\vspace{2\parskip}%
  \stopbreaks
  \noindent
  \ifx\@empty\fromaddress\else
  \hspace*{\longindentation}\fi
  \parbox{90mm}{\raggedright
       \ignorespaces #1\\[6\medskipamount]%
       \ifx\@empty\fromsig
           \fromname
       \else \fromsig \fi\strut}%
   \par\vfill}
\renewcommand*{\cc}[1]{%
  \parbox[t]{\textwidth}{%
    \ixrm\@hangfrom{\ccname: }%
    \ignorespaces #1\strut}}
\renewcommand*{\encl}[1]{% 
  \parbox[t]{\textwidth}{%
    \ixrm\@hangfrom{\enclname: }%
    \ignorespaces #1\strut}\par}
