
English follows Dutch text


RU Briefstijl voor LaTeX

De nieuwe versie ondersteunt een Engelse en een gekleurde versie van het logo.

Op de C&CZ Unix systemen staan in /usr/local/share/texmf/tex/latex/rubrief/ op linux machines onder andere de volgende bestanden:

RUlogo.eps                postscript versie van het logo
RUlogo.jpg                jpg  versie van het logo voor pdflatex
fnwi.tex                  voorbeeldbestand voor afzendergegevens
rubrief.cls               het documentclass bestand voor de rubrief huisstijl
RU_voorbeeld.tex          een voorbeeld brief voor de nieuwe huisstijl

Ook op windows pc's is het mogelijk om de nieuwste LaTeX versie te gebruiken inclusief de rubrief stijl, door de S-schijf aan te koppelen.

Dan vind je de bovenbeschreven bestanden in de directory S:\texlive\texmf-local\tex\latex\rubrief.

Kopieer RU_voorbeeld.tex en fnwi.tex naar een voor jezelf schrijfbare directory. De rubrief stijl is een aangepaste versie van de letter class en is grotendeels ook als zodanig te gebruiken. Bekijk het voorbeeld om te zien welke macro's er zijn om adres- en afzendergegevens vast te leggen. Het is mogelijk het logo in het Engels danwel in het Nederlands af te drukken en men kan kiezen voor een kleurenversie van het logo (optie color). Denk eraan dat als je ook het logo wilt afdrukken dat je zelf dvips moet gebruiken voordat je het dvi-bestand naar een Postscript printer stuurt. Tenslotte is het ook mogelijk direct een pdf versie van een brief te maken met behulp van het commando pdflatex, deze bevat dan al direct het logo.


#######################################################################################################################3

RU letter layout for LaTeX 

The new version supports an English and a colored version of the logo.

On the C&CZ Unix systems one can find in /usr/local/share/texmf/tex/latex/rubrief/ on linux machines the following files:

RUlogo.eps                postscript version of the logo
RUlogo.jpg                jpg  version of the logo for pdflatex
fnwi.tex                  example file for address information
rubrief.cls               the documentclass file for the rubrief letter layout
RU_voorbeeld.tex          an example letter for the new corporate identity

On Windows PC's it is also possible to use the newest version of LaTeX including the rubrief style, by attaching the S-disk.

Then you'll find the above mentioned files in the folder: S:\texlive\texmf-local\tex\latex\rubrief. 

Copy RU_voorbeeld.tex and fnwi.tex to a place where you can write them. 
The rubrief style is an adapted version of the letter class and can be used like that. Look at the
example to see which macro's exist to supply address and sender information. The logo can be printed in Dutch as well as in English and one can choose a color version of the logo (option color). Please note that you'll have to use dvips yourself before sending the dvi-file to a Postscript printer. Finally, it is also possible to make a pdf version of a letter with the command pdflatex, this will then immediately contain the logo. 
